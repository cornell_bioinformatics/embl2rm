#!/usr/bin/perl
use strict;
use warnings;

use Bio::SeqIO;

my $inputFile = $ARGV[0];
my $outFile = $ARGV[1];
unless ($outFile ) 
{
	$outFile= "out_rm.lib";
}
my $logFile = "out_rm.log";

open LOG, ">$logFile";

unless ($inputFile ) 
{
	print "Input file is not defined!\n";
	exit;
}
my $in  = Bio::SeqIO->new(-file => $inputFile ,
                               -format => 'EMBL');

my $out  = Bio::SeqIO->new(-file => ">$outFile" ,
                               -format => 'FASTA');

our %classCode = (uc "DNA transposon" => "DNA",
				  uc "LTR Retrotransposon" => "LTR",
				  uc "Endogenous Retrovirus" => "LTR",
				  uc "Non-LTR Retrotransposon" => "LINE",

					);
my %convertTable = ();

while ( my $seq = $in->next_seq() ) 
{
	my $id= $seq->display_id;
	my $class = "";
	my $subclass = "";
	my $classCode = "Other";
	
	my $spname = "NA";

	eval {
		my $species = $seq->species;

		$spname = $species->node_name;
		$spname =~s/ /_/g;

     };
     if ($@) {
	   $spname = "NA";
       print LOG "species invalid: $id\n";
     };  

	my @KW= $seq->get_keywords("OS");

	if (@KW) 
	{
		my $SINE_Index= getIndex(\@KW, "SINE");
		my $T_Index = getIndex(\@KW, "Transposable Element");
		my $retrovirus_Index = getIndex(\@KW, "Endogenous.Retrovirus");
		my $Satellite_Index = getIndex(\@KW, "Satellite");
		my $simple_Index = getIndex(\@KW, "Simple Repeat");
		my $rRNA_Index = getIndex(\@KW, "rRNA");
		my $snRNA_Index = getIndex(\@KW, "snRNA");
		my $tRNA_Index = getIndex(\@KW, "tRNA");
		my $virus_index = getIndex(\@KW, "Integrated Virus");
		my $other_Index = getIndex(\@KW, "(Repetitive element)|(Repetitive sequence)|(Repetitive DNA)|(Multicopy gene)");

		

		if ($SINE_Index >= 1) 
		{
			$classCode ="SINE";
			$subclass = $KW[$SINE_Index-1];
			$subclass=~s/SINE\w+\///;
		}
		elsif ($SINE_Index == 0) 
		{
			$classCode ="SINE";
		}
		elsif ($T_Index >=2) 
		{
			$class = $KW[$T_Index -1];
			$subclass  = $KW[$T_Index -2];			
			$classCode = $classCode{uc $class};

			unless ($classCode)
			{
				print LOG "Class not recognized: ";
				print LOG ((join "; ", @KW), "\n");
				$classCode = "SINE";
			}
		}
		elsif ($T_Index ==1) 
		{
			$class = $KW[0];
			$classCode = $classCode{uc $class};

			unless ($classCode)
			{
				print LOG "Class not recognized: ";
				print LOG ((join "; ", @KW), "\n");
				$classCode = "Other";
			}
		}
		elsif ($T_Index ==0) 
		{
			$classCode = "Unknown";
		}
		elsif ($retrovirus_Index>=0) 
		{
			$classCode = "LTR";
			$subclass = "Retrovirus";
		}
		elsif ($virus_index>=0) 
		{
			$classCode = "Integrated-virus";
		}
		elsif ($rRNA_Index >=0)
		{
			$classCode = "rRNA";
		}
		elsif ($tRNA_Index >=0)
		{
			$classCode = "tRNA";
		}
		elsif ($snRNA_Index >=0)
		{
			$classCode = "snRNA";
		}
		elsif ($Satellite_Index >=0) 
		{
			$classCode = "Satellite";
		}
		elsif ($simple_Index >=0)
		{
			$classCode = "Simple_repeat";
		}
		elsif ($other_Index >=0) 
		{
			$classCode = "Other";
		}
		else 
		{
			print LOG "Unknown: ";
			print LOG ((join "; ", @KW), "\n");
		}
	}
	else
	{
		print LOG "No KW for $id\n";
	}
	


	$subclass =~s/[ \/]/-/g;
	if ($subclass) 
	{
		$subclass = "/$subclass";
	}
	$seq->display_id("${id}#${classCode}${subclass}");
	$seq->desc("\@$spname");
	$out->write_seq($seq);

}

sub getIndex
{
	my $arrayPointer = shift;
	my $term = shift;
	my $arrayCount = @$arrayPointer + 0;
	for (my $i=0; $i<$arrayCount; $i++) 
	{
		my $word = $$arrayPointer[$i];
		$word =~s/\.$//;
		if ($word =~ /^$term$/i  ) 
		{
			return $i
		}
	}
	return -1;
}
